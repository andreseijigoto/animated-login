import './styles/main.scss'

import Vue from 'vue'
import App from './App.vue'
import Buefy from 'buefy'
import Vuelidate from 'vuelidate'
import VueTheMask from 'vue-the-mask'

import router from './router'
import store from './store'
import i18n from './i18n'
import './setup/vuelidate'

import VueCompositionAPI, { provide } from '@vue/composition-api'
Vue.use(Buefy)
Vue.use(VueCompositionAPI)
Vue.use(Vuelidate)
Vue.use(VueTheMask)

Vue.config.productionTip = false

function createVueInstance () {
  new Vue({
    router,
    store,
    i18n,
    setup () {
      provide('store', store)
    },
    render: (h) => h(App)
  }).$mount('#app')
}

createVueInstance()
