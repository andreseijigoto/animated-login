export default {
  setLoading ({ commit }, value = false) {
    commit('TOGGLE_LOADING', value)
  }
}
