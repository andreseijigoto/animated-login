import Vue from 'vue'
import VueRouter from 'vue-router'

import Access from '@/views/Access/Access'

Vue.use(VueRouter)

const routes = [
  {
    path: '/access',
    name: 'Access',
    component: Access
  },
  {
    path: '/login',
    name: 'Login',
    redirect: { name: 'Access' }
  },
  {
    path: '*',
    redirect: { name: 'Access' }
  }
]

const router = new VueRouter({
  mode: 'history',
  routes,
  scrollBehavior () {
    return { x: 0, y: 0 }
  }
})

export default router
