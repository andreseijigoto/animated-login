import Vue from 'vue'
import VueI18n from 'vue-i18n'
import getBrowserLocale from './util/get-browser-locale'
import supportedLocales from './util/supported-locales'
import dateTimeFormats from '@/i18n/locales/date-time-formats.json'
import numberFormats from '@/i18n/locales/number-formats.json'

Vue.use(VueI18n)

function getStartingLocale () {
  const browserLocale = getBrowserLocale()

  if (Object.keys(supportedLocales).includes(getBrowserLocale())) {
    return browserLocale
  } else {
    return process.env.VUE_APP_I18N_LOCALE || 'en-US'
  }
}

function loadLocaleMessages () {
  const locales = require.context('./locales', true, /[A-Za-z0-9-_,\s]+\.json$/i)
  const messages = {}

  locales.keys().forEach(key => {
    const matched = key.match(/([A-Za-z0-9-_]+)\./i)

    if (matched && matched.length > 1) {
      const locale = matched[1]

      messages[locale] = locales(key)
    }
  })
  return messages
}

export default new VueI18n({
  locale: getStartingLocale(),
  dateTimeFormats,
  numberFormats,
  fallbackLocale: process.env.VUE_APP_I18N_FALLBACK_LOCALE || 'pt-BR',
  messages: loadLocaleMessages(),
})
