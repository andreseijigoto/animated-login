const MININUM_WORD_LENGTH = 2
const MINIMUM_WORDS_AMOUNT = 2

export const fullName = (name) => {
  if (!name) return false

  const nameParts = name.split(' ')

  const amountOfWordsLargerThanMinimumLength =
    nameParts.filter((namePart) => namePart.length >= MININUM_WORD_LENGTH).length

  return amountOfWordsLargerThanMinimumLength >= MINIMUM_WORDS_AMOUNT
}
