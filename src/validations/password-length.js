import { helpers } from 'vuelidate/lib/validators'

export const passwordLength = ({ min, max }) => helpers.withParams(
  { type: 'passwordLength', min, max },
  (password) => {
    if (!password) return false

    return password.length >= min && password.length <= max
  }
)