import i18n from '@/i18n'

export default function () {
  const errorMessages = (field) => {
    if (!field?.$error) return []

    const key = Object.keys(field.$params).find(key => !field[key])
    const params = field.$params[key]

    return [i18n.t(`validation.messages.${key}`, params)]
  }

  return { errorMessages }
}
