const scheduler = typeof setImmediate === 'function' ? setImmediate : setTimeout

export default () => new Promise((resolve) => scheduler(resolve))
