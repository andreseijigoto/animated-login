import { fullName } from '@/validations/full-name'

describe('fullName', () => {
  describe('Check validation for truthy return', () => {
    it('validates dual worded full name', () => {
      const name = 'John Doe'

      expect(fullName(name)).toBeTruthy()
    })

    it('validates short dual worded full name', () => {
      const name = 'Jo Do'

      expect(fullName(name)).toBeTruthy()
    })

    it('validates short dual worded full name', () => {
      const name = 'Jo Do'

      expect(fullName(name)).toBeTruthy()
    })

    it('validates long full name', () => {
      const name = 'John Doe Does Didnt'

      expect(fullName(name)).toBeTruthy()
    })

    it('validates long full name with one lettered words', () => {
      const name = 'John D D Didnt'

      expect(fullName(name)).toBeTruthy()
    })

    it('validates dual worded full name', () => {
      const name = 'John Doe'

      expect(fullName(name)).toBeTruthy()
    })
  })

  describe('Check validation for falsy return', () => {
    it('invalidates one lettered first name', () => {
      const name = 'J Doe'

      expect(fullName(name)).toBeFalsy()
    })

    it('invalidates one lettered last name', () => {
      const name = 'John D'

      expect(fullName(name)).toBeFalsy()
    })

    it('invalidates one worded full name', () => {
      const name = 'John'

      expect(fullName(name)).toBeFalsy()
    })

    it('invalidates one lettered full name', () => {
      const name = 'J'

      expect(fullName(name)).toBeFalsy()
    })

    it('invalidates two lettered full name', () => {
      const name = 'J D'

      expect(fullName(name)).toBeFalsy()
    })

    it('invalidates empty string', () => {
      const name = ''

      expect(fullName(name)).toBeFalsy()
    })

    it('invalidates undefined', () => {
      const name = undefined

      expect(fullName(name)).toBeFalsy()
    })
  })
})
