import { passwordLength } from '@/validations/password-length'

describe('passwordLength', () => {
  describe('Check validation for truthy return', () => {
    it('validates 6 characters password', () => {
      const text = '123456'
      
      expect(passwordLength({ min:6, max: 30 })(text)).toBeTruthy()
    })
    
    it('validates 30 characters password', () => {
      const text = '123456789012345678901234567890'

      expect(passwordLength({ min:6, max: 30 })(text)).toBeTruthy()
    })
  })

  describe('Check validation for falsy return', () => {
    it('invalidates password blank', () => {
      const text = ''

      expect(passwordLength({ min:6, max: 30 })(text)).toBeFalsy()
    })

    it('invalidates password with less than 6 characters', () => {
      const text = '12345'
      
      expect(passwordLength({ min:6, max: 30 })(text)).toBeFalsy()
    })
    
    it('invalidates password with more than 30 characters', () => {
      const text = '1234567890123456789012345678901'

      expect(passwordLength({ min:6, max: 30 })(text)).toBeFalsy()
    })
  })
})
