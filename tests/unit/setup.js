import Vue from 'vue'
import VueCompositionApi from '@vue/composition-api'
import crypto from 'crypto'
import Vuelidate from 'vuelidate'

import './setup/i18n'

Vue.use(VueCompositionApi)
Vue.use(Vuelidate)

Vue.config.productionTip = false

process.env.VUE_APP_API_URL = 'https://base-url.com.br'
process.env.VUE_APP_I18N_LOCALE = 'foo-FOO'

/*
https://github.com/uuidjs/uuid/issues/514
EvCheckbox uses uuid and thats why we need this mock
*/
Object.defineProperty(global.self, 'crypto', {
  value: {
    getRandomValues: arr => crypto.randomBytes(arr.length)
  }
})
