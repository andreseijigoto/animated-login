import Buefy from 'buefy'
import factoryBuilder from '#/factory-builder'
import { createLocalVue } from '@vue/test-utils'

import App from '@/App'

const localVue = createLocalVue()
localVue.use(Buefy)

jest.mock('@vue/composition-api', () => ({
  ...jest.requireActual('@vue/composition-api'),
  inject: jest.fn().mockReturnValue({
    state: {
      system: {
        isLoading: false
      },
      user: {}
    },
    dispatch: jest.fn()
  })
}))

const factory = () => factoryBuilder(App, {
  stubs: {
    'router-view': true,
    'b-loading': true
  },
})

describe('App', () => {
  it('Renders app', () => {
    const wrapper = factory()

    expect(wrapper.exists()).toBeTruthy()
  })
})
