import factoryBuilder from '#/factory-builder'

import Access from '@/views/Access/Access'
import AccessIntro from '@/views/Access/AccessIntro.vue'
import AccessSignin from '@/views/Access/AccessSignin.vue'
import AccessSignup from '@/views/Access/AccessSignup.vue'

jest.mock('@vue/composition-api', () => ({
  ...jest.requireActual('@vue/composition-api'),
  inject: jest.fn().mockReturnValue({
    state: {}
  })
}))

const factory = () => factoryBuilder(Access, {
  stubs: {
    'access-intro': AccessIntro,
    'access-signin': AccessSignin,
    'access-signup': AccessSignup
  }
})

describe('Access', () => {
  it('Renders Access', () => {
    const wrapper = factory()
    
    expect(wrapper.exists()).toBeTruthy()
  })
  
  it('Renders AccessIntro', () => {
    const wrapper = factory()
    const component = wrapper.find('[data-test="access-intro"]')
    
    expect(component.exists()).toBeTruthy()
  })

  it('Renders AccessSignup', () => {
    const wrapper = factory()
    const component = wrapper.find('[data-test="access-signup"]')
    
    expect(component.exists()).toBeTruthy()
  })

  it('Renders AccessSignin', () => {
    const wrapper = factory()
    const component = wrapper.find('[data-test="access-signin"]')
    
    expect(component.exists()).toBeTruthy()
  })
  
  it('Not renders AccessModalFacebook', () => {
    const wrapper = factory()
    const component = wrapper.find('[data-test="access-modal"]')
    
    expect(component.exists()).toBeFalsy()
  })
    
  describe('Check computed introMode', () => {  
    it('when swipe is true', () => {
      const wrapper = factory()
      wrapper.vm.swipe = true
      
      expect(wrapper.vm.introMode).toEqual('signin')
    })
    
    it('When swipe is false', () => {
      const wrapper = factory()
      wrapper.vm.swipe = false
      
      expect(wrapper.vm.introMode).toEqual('signup')
    })
  })
    
  describe('Check computed isSigninActive', () => {  
    it('When swipe is true', () => {
      const wrapper = factory()
      wrapper.vm.swipe = true
      
      expect(wrapper.vm.isSigninActive).toBeFalsy()
    })
    
    it('When swipe is false', () => {
      const wrapper = factory()
      wrapper.vm.swipe = false
      
      expect(wrapper.vm.isSigninActive).toBeTruthy()
    })
  })
    
  describe('Check computed isSignupActive', () => {
    it('When swipe is true', () => {
      const wrapper = factory()
      wrapper.vm.swipe = true
      
      expect(wrapper.vm.isSignupActive).toBeTruthy()
    })
    
    it('When swipe is false', () => {
      const wrapper = factory()
      wrapper.vm.swipe = false
      
      expect(wrapper.vm.isSignupActive).toBeFalsy()
    })
  })

  describe('Check method toogleSwipe', () => {
    it('When swipe is true and triggered', async () => {
      const wrapper = factory()
      await wrapper.vm.toggleSwipe()
      
      expect(wrapper.vm.swipe).toBeFalsy()
    })

    it('When swipe is false and triggered', async () => {
      const wrapper = factory()
      wrapper.vm.swipe = false

      await wrapper.vm.toggleSwipe()
      
      expect(wrapper.vm.swipe).toBeTruthy()
    })
  })
})
