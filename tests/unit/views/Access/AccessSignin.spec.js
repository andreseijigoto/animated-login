import factoryBuilder from '#/factory-builder'

import AccessSignin from '@/views/Access/AccessSignin'

const factory = (args = {}) => factoryBuilder(AccessSignin, {
  propsData: { ...args },
  stubs: {
    'b-button': true,
    'b-field': true,
    'b-input': true
  }
})

describe('AccessSignin', () => {
  describe('Check if components renders', () => {
    it('renders AccessSignin', () => {
      const wrapper = factory()
      
      expect(wrapper.exists()).toBeTruthy()
    })

    it('renders b-field of email', () => {
      const wrapper = factory()
      const component = wrapper.find('[data-test="field-email"]')
      
      expect(component.exists()).toBeTruthy()
    })

    it('renders b-input of email', () => {
      const wrapper = factory()
      const component = wrapper.find('[data-test="input-email"]')
      
      expect(component.exists()).toBeTruthy()
    })

    it('renders b-field of password', () => {
      const wrapper = factory()
      const component = wrapper.find('[data-test="field-password"]')
      
      expect(component.exists()).toBeTruthy()
    })

    it('renders b-input of password', () => {
      const wrapper = factory()
      const component = wrapper.find('[data-test="input-password"]')
      
      expect(component.exists()).toBeTruthy()
    })
  })

  describe('Check correct components properties', () => {
    it('check correct label of input email', () => {
      const wrapper = factory()
      const component = wrapper.find('[data-test="field-email"]')
      
      expect(component.attributes().label).toEqual('AccessSignin.form.email')
    })

    it('check correct placeholder of input email', async() => {
      const wrapper = await factory()
      const component = wrapper.find('[data-test="input-email"]')
      
      expect(component.attributes().placeholder).toEqual('AccessSignin.form.emailPlaceholder')
    })

    it('check correct label of input password', () => {
      const wrapper = factory()
      const component = wrapper.find('[data-test="field-password"]')
      
      expect(component.attributes().label).toEqual('AccessSignin.form.password')
    })

    it('check correct placeholder of input password', () => {
      const wrapper = factory()
      const component = wrapper.find('[data-test="input-password"]')
      
      expect(component.attributes().placeholder).toEqual('AccessSignin.form.passwordPlaceholder')
    })
  })

  describe('Check correct components properties', () => {
    it('check if submit button became enabled', async () => {
      const wrapper = factory()
      
      wrapper.find('[data-test="input-password"]').vm.$emit('input', 123456)
      wrapper.find('[data-test="input-email"]').vm.$emit('input', 'teste@teste.com.br')
      
      await wrapper.vm.$nextTick()
      
      const component = wrapper.find('[data-test="button-submit"]')
            
      expect(component.props('disabled')).toBeFalsy()
    })
    
    it('check if submit button became disabled', async () => {
      const wrapper = factory()
      const component = wrapper.find('[data-test="button-submit"]')
      
      expect(component.attributes().disabled).toBeTruthy()
    })
  })

  describe('Check methods', () => {
    it('triggers validation', () => {
      const wrapper = factory()
      const validationSpy = jest.spyOn(wrapper.vm.$v, '$touch', 'get')

      wrapper.find('[data-test="button-submit"]').vm.$emit('click')

      expect(validationSpy).toHaveBeenCalled()
    })
  })
})
