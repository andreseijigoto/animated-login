import factoryBuilder from '#/factory-builder'
import AccessSignup from '@/views/Access/AccessSignup'

const factory = () => factoryBuilder(AccessSignup, {
  stubs: {
    'b-button': true,
    'b-field': true,
    'b-input': true
  }
})

describe('AccessSignup', () => {
  it('Renders AccessSignup', () => {
    const wrapper = factory()
    
    expect(wrapper.exists()).toBeTruthy()
  })

  it('Renders b-field of name', () => {
    const wrapper = factory()
    const component = wrapper.find('[data-test="field-name"]')
    
    expect(component.exists()).toBeTruthy()
  })

  it('Renders b-input of name', () => {
    const wrapper = factory()
    const component = wrapper.find('[data-test="input-name"]')
    
    expect(component.exists()).toBeTruthy()
  })

  it('Renders b-field of phone', () => {
    const wrapper = factory()
    const component = wrapper.find('[data-test="field-phone"]')
    
    expect(component.exists()).toBeTruthy()
  })

  it('Renders b-input of phone', () => {
    const wrapper = factory()
    const component = wrapper.find('[data-test="input-phone"]')
    
    expect(component.exists()).toBeTruthy()
  })

  it('Renders b-field of email', () => {
    const wrapper = factory()
    const component = wrapper.find('[data-test="field-email"]')
    
    expect(component.exists()).toBeTruthy()
  })

  it('Renders b-input of email', () => {
    const wrapper = factory()
    const component = wrapper.find('[data-test="input-email"]')
    
    expect(component.exists()).toBeTruthy()
  })

  it('Renders b-field of password', () => {
    const wrapper = factory()
    const component = wrapper.find('[data-test="field-password"]')
    
    expect(component.exists()).toBeTruthy()
  })

  it('Renders b-input of password', () => {
    const wrapper = factory()
    const component = wrapper.find('[data-test="input-password"]')
    
    expect(component.exists()).toBeTruthy()
  })

  it('Renders b-field of passwordConfirm', () => {
    const wrapper = factory()
    const component = wrapper.find('[data-test="field-passwordConfirm"]')
    
    expect(component.exists()).toBeTruthy()
  })

  it('Renders b-input of passwordConfirm', () => {
    const wrapper = factory()
    const component = wrapper.find('[data-test="input-passwordConfirm"]')
    
    expect(component.exists()).toBeTruthy()
  })

  it('Renders b-field of name', () => {
    const wrapper = factory()
    const component = wrapper.find('[data-test="button-submit"]')
    
    expect(component.exists()).toBeTruthy()
  })

  describe('Check correct components properties', () => {
    it('check correct label of input name', () => {
      const wrapper = factory()
      const component = wrapper.find('[data-test="field-name"]')
      
      expect(component.attributes().label).toEqual('AccessSignup.form.name')
    })

    it('check correct placeholder of input name', () => {
      const wrapper = factory()
      const component = wrapper.find('[data-test="input-name"]')
      
      expect(component.attributes().placeholder).toEqual('AccessSignup.form.namePlaceholder')
    })

    it('check correct label of input phone', () => {
      const wrapper = factory()
      const component = wrapper.find('[data-test="field-phone"]')
      
      expect(component.attributes().label).toEqual('AccessSignup.form.phone')
    })

    it('check correct placeholder of input phone', () => {
      const wrapper = factory()
      const component = wrapper.find('[data-test="input-phone"]')
      
      expect(component.attributes().placeholder).toEqual('AccessSignup.form.phonePlaceholder')
    })

    it('check correct label of input email', () => {
      const wrapper = factory()
      const component = wrapper.find('[data-test="field-email"]')
      
      expect(component.attributes().label).toEqual('AccessSignup.form.email')
    })

    it('check correct placeholder of input email', () => {
      const wrapper = factory()
      const component = wrapper.find('[data-test="input-email"]')
      
      expect(component.attributes().placeholder).toEqual('AccessSignup.form.emailPlaceholder')
    })

    it('check correct label of input password', () => {
      const wrapper = factory()
      const component = wrapper.find('[data-test="field-password"]')
      
      expect(component.attributes().label).toEqual('AccessSignup.form.password')
    })

    it('check correct placeholder of input password', () => {
      const wrapper = factory()
      const component = wrapper.find('[data-test="input-password"]')
      
      expect(component.attributes().placeholder).toEqual('AccessSignup.form.passwordPlaceholder')
    })

    it('check correct label of input passwordConfirm', () => {
      const wrapper = factory()
      const component = wrapper.find('[data-test="field-passwordConfirm"]')
      
      expect(component.attributes().label).toEqual('AccessSignup.form.passwordConfirm')
    })

    it('check correct placeholder of input passwordConfirm', () => {
      const wrapper = factory()
      const component = wrapper.find('[data-test="input-passwordConfirm"]')
      
      expect(component.attributes().placeholder).toEqual('AccessSignup.form.passwordPlaceholder')
    })

    it('renders b-field of name', () => {
      const wrapper = factory()
      const component = wrapper.find('[data-test="button-submit"]')
      
      expect(component.exists()).toBeTruthy()
    })
  })

  describe('Check methods', () => {
    it('triggers validation', () => {
      const wrapper = factory()
      const validationSpy = jest.spyOn(wrapper.vm.$v, '$touch', 'get')

      wrapper.find('[data-test="button-submit"]').vm.$emit('click')

      expect(validationSpy).toHaveBeenCalled()
    })
  })
})