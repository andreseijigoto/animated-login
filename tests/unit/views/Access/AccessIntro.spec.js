import factoryBuilder from '#/factory-builder'

import AccessIntro from '@/views/Access/AccessIntro'
import AccessShapes from '@/views/Access/components/AccessShapes.vue'

const factory = () => factoryBuilder(AccessIntro, {
  stubs: {
    'b-button': true,
    'access-shapes': AccessShapes
  }
});

describe('AccessIntro', () => {
  describe('Check if components renders', () => {
    it('renders AccessIntro', () => {
      const wrapper = factory()
      
      expect(wrapper.exists()).toBeTruthy()
    })
    
    it('renders b-button', () => {
      const wrapper = factory()
      const component = wrapper.find("[data-test='button-swipe']")
      
      expect(component.exists()).toBeTruthy()
    })
    
    it('renders AccessShapes', () => {
      const wrapper = factory()
      const component = wrapper.find('[data-test="access-shapes"]')
      
      expect(component.exists()).toBeTruthy()
    })
  })

  describe('Check correct components properties', () => {
    it('check correct buttonText value with mode equal signup', () => {
      const wrapper = factory({ mode: 'signup'})
      
      expect(wrapper.vm.buttonText).toEqual(`AccessIntro.${wrapper.vm.mode}.buttonText`)
    })
    
    it('check correct buttonText value with mode equal signin', () => {
      const wrapper = factory({ mode: 'signin'})
      
      expect(wrapper.vm.buttonText).toEqual(`AccessIntro.${wrapper.vm.mode}.buttonText`)
    })
  
    it('check correct text value with mode equal signup', () => {
      const wrapper = factory({ mode: 'signup'})
      
      expect(wrapper.vm.text).toEqual(`AccessIntro.${wrapper.vm.mode}.text`)
    })
    
    it('check correct text value with mode equal signin', () => {
      const wrapper = factory({ mode: 'signin'})
      
      expect(wrapper.vm.text).toEqual(`AccessIntro.${wrapper.vm.mode}.text`)
    })
  
    it('check correct title value with mode equal signup', () => {
      const wrapper = factory({ mode: 'signup'})
      
      expect(wrapper.vm.title).toEqual(`AccessIntro.${wrapper.vm.mode}.title`)
    })
    
    it('check correct title value with mode equal signin', () => {
      const wrapper = factory({ mode: 'signin'})
      
      expect(wrapper.vm.title).toEqual(`AccessIntro.${wrapper.vm.mode}.title`)
    })
  })

  describe('Check methods', () => {
    it('check if when mounted call function setText', async () => {
      const setText = jest.spyOn(AccessIntro.methods, 'setText')
      factory()
      
      expect(setText).toHaveBeenCalled()
    })
  })
})
