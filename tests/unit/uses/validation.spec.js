import factoryBuilder from '#/factory-builder'
import useValidation from '@/uses/validation'

jest.mock('@/i18n', () => ({
  t: (str, params) => `${str}${JSON.stringify(params)}`
}))

const DummyComponent = {
  name: 'dummy-component',
  template: '<p>dummy</p>',
  setup () {
    return useValidation()
  }
}

const factory = () => factoryBuilder(DummyComponent)

describe('useValidation', () => {
  describe('#errorMessages', () => {
    it('interpolates an error message for first failing rule using validator params', () => {
      const { vm } = factory()
      const field = {
        $error: true,
        $params: {
          ruleOne: { min: 1, max: 15 },
          ruleTwo: { type: 'required' }
        },
        ruleOne: false,
        ruleTwo: false
      }

      expect(vm.errorMessages(field)).toEqual(
        ['validation.messages.ruleOne{"min":1,"max":15}']
      )
    })
  })

  describe('when there is no error on field', () => {
    it('returns []', () => {
      const { vm } = factory()
      const field = { $error: false }

      expect(vm.errorMessages(field)).toEqual([])
    })
  })

  describe('when field is falsy', () => {
    it('returns []', () => {
      const { vm } = factory()
      const field = undefined

      expect(vm.errorMessages(field)).toEqual([])
    })
  })
})
