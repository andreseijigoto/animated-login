import getBrowserLocale from '@/i18n/util/get-browser-locale'

describe('getBrowserLocale', () => {
  describe('when navigator.languages and navigator.language are undefined', () => {
    it('should return undefined', () => {
      const navigatorLanguagesGetter = jest.spyOn(window.navigator, 'languages', 'get')
      const navigatorLanguageGetter = jest.spyOn(window.navigator, 'language', 'get')

      navigatorLanguagesGetter.mockReturnValue(undefined)
      navigatorLanguageGetter.mockReturnValue('')

      expect(getBrowserLocale()).toBe(undefined)
    })
  })

  describe('when navigator.languages is empty but navigator.language exists', () => {
    it('should return trimmed navigator.language', () => {
      const navigatorLanguagesGetter = jest.spyOn(window.navigator, 'languages', 'get')
      const navigatorLanguageGetter = jest.spyOn(window.navigator, 'language', 'get')

      navigatorLanguagesGetter.mockReturnValue(undefined)
      navigatorLanguageGetter.mockReturnValue('en-US')

      expect(getBrowserLocale()).toBe('en-US')
    })
  })

  describe('when navigator.languages exists', () => {
    it('returns first language from languages array', () => {
      const navigatorLanguagesGetter = jest.spyOn(window.navigator, 'languages', 'get')
      const navigatorLanguageGetter = jest.spyOn(window.navigator, 'language', 'get')

      navigatorLanguagesGetter.mockReturnValue(['pt-BR', 'pt', 'en-US', 'en'])
      navigatorLanguageGetter.mockReturnValue('pt')

      expect(getBrowserLocale()).toBe('pt-BR')
    })
  })
})
