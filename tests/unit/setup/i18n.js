import Vue from 'vue'
import { config } from '@vue/test-utils'

config.mocks.$t = (string, params) => `${string}${params ? JSON.stringify(params) : ''}`
config.mocks.$tc = (string, params) => `${string}${params ? JSON.stringify(params) : ''}`
config.mocks.$d = (string, format) => `${string} formatted as ${format}`
config.mocks.$n = (value) => `${value}`

Vue.component('I18n', {
  name: 'I18n',
  template: '<template><slot /></template>'
})
