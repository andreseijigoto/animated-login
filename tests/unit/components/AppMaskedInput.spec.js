import factoryBuilder from '#/factory-builder'
import AppMaskedInput from '@/components/AppMaskedInput'

jest.mock('@vue/composition-api', () => ({
  ...jest.requireActual('@vue/composition-api'),
  inject: jest.fn()
}))

const factory = (args) => factoryBuilder(AppMaskedInput, {
  propsData: { ...args },
  stubs: {
    'b-icon': true
  }
})

describe('AppBox', () => {
  describe('Check if components renders', () => {
    it('renders AppMaskedInput', () => {
      const wrapper = factory()
      
      expect(wrapper.exists()).toBeTruthy()
    })

    describe('Component the mask', () => {
      it('renders TheMask component', () => {
        const wrapper = factory()
        const component = wrapper.find('[data-test="the-mask"]')
        
        expect(component.exists()).toBeTruthy()
      })

      it('renders TheMask component with mask property', () => {
        const wrapper = factory({mask: '#####-####'})
        const component = wrapper.find('[data-test="the-mask"]')

        expect(component.attributes().mask).toEqual('#####-####')
      })

      it('renders TheMask component with value property', () => {
        const wrapper = factory({value: 'input value'})
        const component = wrapper.find('[data-test="the-mask"]')

        expect(component.attributes().value).toEqual('input value')
      })
    })

    describe('Componens BIcon', () => {
      it('renders BIcon left', () => {
        const wrapper = factory({ icon: 'teste' })
        const iconLeft = wrapper.find('[data-test="icon-left"]')

        expect(iconLeft.exists()).toBeTruthy()
      })
      
      it('check BIcon left icon property', () => {
        const wrapper = factory({ icon: 'teste' })
        const iconLeft = wrapper.find('[data-test="icon-left"]')

        expect(iconLeft.attributes().icon).toEqual('teste')
      })
      
      it('renders BIcon right', () => {
        const wrapper = factory({ iconRight: 'teste' })
        const iconRight = wrapper.find('[data-test="icon-right"]')

        expect(iconRight.exists()).toBeTruthy()
      })
    
      it('check BIcon right icon property', () => {
        const wrapper = factory({ iconRight: 'teste' })
        const iconRight = wrapper.find('[data-test="icon-right"]')

        expect(iconRight.attributes().icon).toEqual('teste')
      })
    })
  })
})
    