import Buefy from 'buefy'
import Vuelidate from 'vuelidate'
import VueI18n from 'vue-i18n'
import { createLocalVue, shallowMount } from '@vue/test-utils'

const localVue = createLocalVue()
localVue.use(Buefy)
localVue.use(VueI18n)
localVue.use(Vuelidate)

export default (component, args = {}) => shallowMount(component, {
  localVue,
  propsData: args.propsData,
  computed: args.computed,
  mixins: args.mixins,
  mocks: args.mocks,
  slots: args.slots,
  stubs: args.stubs,
  listeners: args.listeners,
  directives: {
    mask () {},
    ...args.directives
  }
})
