module.exports = {
  preset: '@vue/cli-plugin-unit-jest',
  transformIgnorePatterns: ['/node_modules'],
  transform: {
    '^.+\\.vue$': '@vue/vue2-jest'
  },
  clearMocks: true,
  restoreMocks: true,
  setupFiles: [
    '<rootDir>/tests/unit/setup'
  ],
  collectCoverage: true,
  coverageDirectory: '<rootDir>/tests/unit/coverage',
  coveragePathIgnorePatterns: [
    'src/i18n/index.js',
    'src/i18n/locales',
    'tests/unit/factory-builder.js',
    'setup/*'
  ],
  coverageThreshold: {
    global: {
      branches: 90,
      functions: 90,
      lines: 100,
      statements: -0
    }
  },
  moduleNameMapper: {
    '^#/(.*)$': '<rootDir>/tests/unit/$1'
  }
}
