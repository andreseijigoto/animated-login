const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true
})

module.exports = {
  lintOnSave: 'warning',

  css: {
    loaderOptions: {
      sass: {
        additionalData: '@import "@/styles/_variables.scss";'
      }
    }
  },

  devServer: {
    allowedHosts: 'all'
  },

  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: false
    }
  }
}